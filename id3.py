import numpy as np, pandas as pd, scipy.stats as st
from collections import Counter

def sk_decision_tree(x, y):
    clf = DecisionTreeClassifier(criterion = "entropy") 
    clf.fit(x.values, y.values)
    y_pred = clf.predict(x) 
    return accuracy_score(u,y_pred)*100

def id3_entropy(x):
    _, freqs = np.unique(x, return_counts=True)
    probs = freqs / len(x)
    return -probs.dot(np.log2(probs))

def id3_information_gain(x, y, split=None):
    if split is not None:
        e = len(x[x <= split]) * id3_entropy(y[x <= split]) + len(x[x > split]) * id3_entropy(y[x > split])
    else:
        data_values, data_freqs = np.unique(x, return_counts=True)
        x_counts = dict(zip(data_values, data_freqs))
        e = 0.0
        for x_val, x_val_count in x_counts.items():
            e += x_val_count * id3_entropy(y[x == x_val])
    return id3_entropy(y) - e / len(x)

def hypothesis_test(attribute_data, labels, p_threshold=None):
    # Get label frequencies
    data_values, data_freqs = np.unique(labels, return_counts=True)
    label_counts = dict(zip(data_values, data_freqs))
    # Get attribute value frequencies
    data_values, data_freqs = np.unique(attribute_data, return_counts=True)
    attr_val_counts = dict(zip(data_values, data_freqs))
    # Calculate length of data (outside of loops below)
    total_count = len(labels)

    # k and m will be used for degrees of freedom in chi-squared call
    # k unique classes
    k = len(label_counts)
    # m unique attribute values
    m = len(attr_val_counts)

    statistic = 0.0
    for attr_val, attr_val_count in attr_val_counts.items():
        attr_val_ratio = attr_val_count / total_count
        # Get corresponding label frequencies within this attribute value
        data_values, data_freqs = np.unique(labels[attribute_data == attr_val], return_counts=True)
        label_counts_attr_val = dict(zip(data_values, data_freqs))
        for label_attr_val, label_count_attr_val in label_counts_attr_val.items():
            # Expected label count is the probability of the attribute value by the
            # probability of the label within the attribute
            exp_label_count_attr_val = attr_val_ratio * label_counts[label_attr_val]
            # Calculate the Chi-square statistic
            statistic += (label_count_attr_val - exp_label_count_attr_val)**2 / exp_label_count_attr_val

    # Calculate the p value from the chi-square distribution CDF
    p_value = 1 - st.chi2.cdf(statistic, df=(m-1)*(k-1))

    return p_value < p_threshold


class DecisionTree:
    # the prediction at each node
    label = None
    # Specifies which attribute to split on for the children
    attribute = None
    # Value for that attribute for this child
    attribute_value = None
    # A list of child nodes
    children = None
    #Parent
    parent = None

    def __init__(self, data, labels, attributes, value=None, parent=None):

        if value is not None:
            self.attribute_value = value

        if parent is not None:
            self.parent = parent

        # If labels are all the same, set label and return
        if np.all(labels[:] == labels[0]):
            self.label = labels[0]
            return

        # If corresponding attribute values are the same on every example just pick the last label and return
        if np.all(data[:] == data[0]):
            self.label, _ = Counter(list(labels)).most_common(1)[0]
            return

        self.build(data, labels, attributes)
        return

    def build(self, data, labels, attributes):
        self.find_split_attr(data, labels, attributes)
        best_attribute_column = attributes.index(self.attribute)
        attribute_data = data[:, best_attribute_column]

        # The child trees will be passed data for all attributes except the split attribute
        child_attributes = attributes[:]
        child_attributes.remove(self.attribute)

        self.children = []
        for val in np.unique(attribute_data):
            # Create children for data where the split attribute == val for each unique value for the attribute
            child_data = np.delete(data[attribute_data == val,:], best_attribute_column,1)
            child_labels = labels[attribute_data == val]
            self.children.append(DecisionTree(child_data, child_labels, child_attributes, value=val, parent=self))

    def find_split_attr(self, data, labels, attributes):
        best_gain = float('-inf')
        for attribute in attributes:
            attribute_data = data[:, attributes.index(attribute)]
            best_cont_gain = float('-inf')

            #if the attribite is continious find the threshold by the method provided
            if isinstance(np.all(attribute_data),int):
                a = np.unique(attribute_data)
                for i in range(len(a) - 1):
                    split = 0.0
                    split = a[i] + a[i+1] / 2
                    cont_gain = id3_information_gain(attribute_data, labels, split)
                    if cont_gain > best_cont_gain:
                        best_cont_gain = cont_gain
                gain = best_cont_gain

            else:
                gain = id3_information_gain(attribute_data, labels)

            if gain > best_gain:
                best_gain = gain
                self.attribute = attribute
        return

    def classify(self, data, dset=None):       

        if self.children is None:
            return self.label

        labels = np.empty(len(data), dtype=object)

        #For each child node find the ones that have the split values same as the unquie attribute values for the current dataset.
        for child in self.children:
            for attr_val in np.unique(data[self.attribute].values):
                if attr_val == child.attribute_value:

                    # Get the array indexes where the split attibute value = child attribute value and pass the subsets to children for classification
                    child_attr_val_idx = data[self.attribute] == child.attribute_value
                    labels[child_attr_val_idx] = child.classify(data[child_attr_val_idx], dset)

                    #If we are using the validation set, then prune the tree using chi-squared test
                    if dset == 'dev' and np.all(labels) is not None:
                        no_prune = hypothesis_test(data[self.attribute].values[child_attr_val_idx], labels[child_attr_val_idx], p_threshold=1.0)
                        
                        #If we have to prune then the we find the most common label as the prediction and set this as the leaf node
                        if not no_prune:
                            self.label, _ = Counter(list(labels)).most_common(1)[0]
                            self.children = None
                            return self.label
                        else:
                            return labels

        return labels
    
    def score(self, y_pred, y):
        return len([i for i, val in enumerate(y_pred) if val == y[i]])/len(y)

def main():
    print('ID3 output: ')
    cols = ["age", "workclass", "education", "relationship", "profession", "race", "gender", "workhours", "nationality", "income"]
    dev_data = pd.read_csv('income.dev.txt', header = None, names = cols)
    test_data = pd.read_csv('income.test.txt', header = None, names = cols)
    train_data = pd.read_csv('income.train.txt', header = None, names = cols)

    #Removing the duplicate rows from the training data but leaving the last duplicate entry for data consistancy
    train_data = pd.DataFrame(data=train_data[~train_data.duplicated(subset=cols[:len(cols)-1], keep='last')].values, columns=cols)

    #Doing the data preprocessing over the given data
    data = pd.concat([train_data, dev_data, test_data])
    X, Y = data.drop('income', axis=1), data['income']

    x_train, y_train = X[:len(train_data)], Y[:len(train_data)]
    x_dev, y_dev = X[len(train_data):len(dev_data)+len(train_data)], Y[len(train_data):len(dev_data)+len(train_data)]
    x_test, y_test = X[len(dev_data)+len(train_data):], Y[len(dev_data)+len(train_data):]
    df = x_train.duplicated(keep = False)
    y_train[df == True] = y_train.iloc[0]

    clf = DecisionTree(x_train.values, y_train.values, list(x_train.columns.values))
    score_train = clf.score(clf.classify(x_train), y_train)
    print('1) The training accuracy before pruning = ' + str(score_train))
    score_dev = clf.score(clf.classify(x_dev), y_dev)
    print('2) The dev accuracy before pruning = ' + str(score_dev))
    score_test = clf.score(clf.classify(x_test), y_test)
    print('3) The test accuracy before pruning = ' + str(score_test))
    score_dev = clf.score(clf.classify(x_dev, 'dev'), y_dev)
    print('4) The dev accuracy after pruning = ' + str(score_dev))
    score_train = clf.score(clf.classify(x_train), y_train)
    print('5) The training accuracy after pruning = ' + str(score_train))
    score_test = clf.score(clf.classify(x_test), y_test)
    print('6) The test accuracy after pruning = ' + str(score_test))

if __name__ == "__main__": main()